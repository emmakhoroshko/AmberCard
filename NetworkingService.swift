//
//  NetworkingService.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 17.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import RealmSwift
import ObjectMapper


typealias JSON = [String: Any]

class NetworkingService {
    
    
    let URL_AUTHORIZATION = "http://138.68.68.166:9999/api/1/login"
    let URL_GET_ALL_CONTENT = "http://138.68.68.166:9999/api/1/content"
    let URL_DELETE_FAVOURITE_PLACE = "http://138.68.68.166:9999/api/1/favorites/"
    let URL_ADD_PLACE_TO_FAVOURITE = "http://138.68.68.166:9999/api/1/favorites/"
    let URL_DOWNLOAD_ICON = "http://138.68.68.166:9999/"
    
    let realm = try! Realm()
    static let shared = NetworkingService()
    private init() {}
    
    let headers: HTTPHeaders = [
        "Authorization": "Token 88428fb28837e841dc949c13a0550c3e2c4645ad"
    ]
    public var giveAsses = false
    func getAllPlaces(completion:@escaping (Bool) -> ()) {
       
        Alamofire.request(URL_GET_ALL_CONTENT, headers: self.headers).responseJSON { response in
      //  request("https://api.myjson.com/bins/16bg6v").responseJSON {
          //  response in
            if let json = response.result.value {
                if let jsondata = json as? [String: Any] {
                    do {
                        print(jsondata)
                        try! self.realm.write() { // 2
                            
                            if let categoryDefaults = jsondata["categories"] as? [[String: Any]]
                            {
                                for category  in categoryDefaults {
                                    
                                    //let newCategory = PlacesCategories()
                                    if let newCategory = Mapper<PlacesCategories>().map(JSON: category) {
                                        self.realm.add(newCategory,update: true)
                                    }
                                }
                            }
                        }
                    }
                    do {
                        try! self.realm.write() {
                            if let PointsDefaults = jsondata["points"] as? [[String: Any]]
                            {
                                for point in PointsDefaults { // 4
                                    if let newPoint = Mapper<PlaceModel>().map(JSON: point) {
                                        
                                        let photos =  point["photos"] as? [String]
                                        
                                        for picture in photos! {
                                            let photo = PhotosModel()
                                            photo.place_id = (point["id"] as? Int)!
                                            photo.photo = picture
                                            newPoint.photos.append(photo)
                                        }
                                        let categoryArray = point["category_id"] as? [Int]
                                        
                                        for item in categoryArray! {
                                            let category = LinkToCategory()
                                            category.value = item
                                            newPoint.category_id.append(category)
                                        }
                                        
                                        self.realm.add(newPoint, update: true)
                                    }
                                    completion(true)
                                }
                            }
                        }
                    }
                }
            }            
        }
    }
    
    func loginAccept(name:String , password: String ,completion:@escaping (Bool) -> ()) {
        

        let params : Parameters = ["username": name ,"password" : password]
        Alamofire.request(URL_AUTHORIZATION, method: .post, parameters: params).validate().responseJSON { response in
            
            if let json = response.result.value {
                 if let jsondata = json as? [String: Any] {
                     if jsondata["token"] != nil {
                        completion(true)
                    }else{
                        self.giveAsses = false
                        completion(false)
                    }
                }else{
                    completion(false)
                }
                
            }else{
                completion(false)
            }
        }
    }
    
    func downloadIcon(_ url: String, defaultIcon: UIImage,  completion:@escaping (UIImage) -> ()) {
        
        let urlPath = ("\(URL_DOWNLOAD_ICON)\(url)")
        Alamofire.request(urlPath).responseImage  { response  in
            if let imag = response.result.value {
                print(imag)
                completion (imag)
            }else{
                completion (defaultIcon)
            }
        }
    }
    
    func addPlaceToFav ( place: PlaceModel, completion:@escaping (Bool) -> () ) {
        
        let urlPath = ("\(URL_ADD_PLACE_TO_FAVOURITE)\(place.id)")
       Alamofire.request(urlPath, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { response in
            if let  json = response.result.value  {
                 print(json)
                if let jsondata = json as? [[String: Any]] {
                    do {
                        try! self.realm.write() {
                            
                        place.loved = true
                        self.realm.add(place, update: true)
                        print(jsondata)

                completion (true)
                        }
                    }catch{
                      completion (false)
                }
            }
        }
    }
    }
    
    
    func deletePlaceFromFav ( place: PlaceModel, completion:@escaping (String) -> () ) {
        
        let urlPath = ("\(URL_DELETE_FAVOURITE_PLACE)\(place.id)")
        Alamofire.request(urlPath, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: self.headers).responseJSON { response in
            if let  json = response.result.value  {
                print(json)
                if let jsondata = json as? [[String: Any]] {
                    do {
                        try! self.realm.write() {
                            place.loved = false
                            self.realm.add(place, update: true)
                            print(jsondata)
                            
                            completion ("success")
                        }
                    }
                }
            }
        }
    }

    
    
}
