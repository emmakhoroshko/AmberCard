//
//  RoundButton.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 23.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import Foundation

class CustomButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    func commonInit(){
        self.layer.cornerRadius = 4
        self.titleLabel?.font = UIFont.acH3Font()
        self.backgroundColor = UIColor.acStrawberry
        self.tintColor = UIColor.acWhiteTwo
    }


}
