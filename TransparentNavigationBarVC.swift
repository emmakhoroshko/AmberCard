//
//  TransparentNavigationBarVC.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 26.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

class TransparentNavigationBarVC: UIViewController {
    
    
    
  var scrollOffset = CGPoint.init(x: 0.0, y: -80.0) //место где сменяется пустой навигейшн бар на заполненный
    override func viewDidLoad() {
        super.viewDidLoad()
self.createNavigationBar()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createNavigationBar (){
       
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.navigationController?.navigationBar.tintColor = UIColor.acStrawberry
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.acStrawberry
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.acStrawberry
    }
    
    func tappedBack () {
        
        
    }
    func addTapped () {
        print("addTapped")
  
    }
    
    func deleteTapped () {
        print("deleteTapped")
        
        }
    
    
    func createFullNavigationBar (scroll: CGPoint){
        self.createNavigationBar()
        self.navigationController?.navigationBar.alpha = 1/scrollOffset.y * scroll.y - 0.9
        
    }
    func createTransparentNavigationBar (scroll: CGPoint){
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.acStrawberry
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.acStrawberry
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.alpha = -1/scrollOffset.y * scroll.y + 1.1
        self.navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
}
