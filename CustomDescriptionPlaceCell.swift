//
//  CustomDescriptionPlaceCell.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 12.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

class CustomDescriptionPlaceCell: UITableViewCell {
    
    
    @IBOutlet weak var placeTypeIcon: UIImageView!
    @IBOutlet weak var placeType: UILabel!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var placeDescription: UILabel!
    
    
    func createCustonCell(place:PlaceModel, categ: PlacesCategories) {
        
        self.placeName.text = place.name == "" ? "":"\(place.name)"
        self.placeDescription.text = place.description_text == "" ? "":"\(place.description_text)"
        self.placeType.text = categ.name == "" ? "":"\(categ.name)"
        NetworkingService.shared.downloadIcon(categ.icon, defaultIcon: #imageLiteral(resourceName: "defImg"), completion: { (img) in
            self.placeTypeIcon.image = img
        })
    }
}
