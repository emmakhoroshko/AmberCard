//
//  PointAnnotation.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 24.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

class PointAnnotation: NSObject, YMKAnnotation {
    public var  title_tmp: String = ""
    public var subTitle_tmp: String = ""
    public var coordinate_tmp = YMKMapCoordinate()
    var image = YMKAnnotationImage ()
    
    func title() -> String!{
        return title_tmp
    }
    func subtitle() -> String! {
        return subTitle_tmp
    }
    func coordinate() -> YMKMapCoordinate {
        return coordinate_tmp
    }
    
    
    
    
}
