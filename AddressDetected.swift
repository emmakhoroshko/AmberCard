//
//  AddressDetected.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 24.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import Foundation
import CoreLocation

class AddressDetected {
    
static let shared = AddressDetected()
func getPlacemark(forLocation location: CLLocation, completionHandler: @escaping (CLPlacemark?, String?) -> ()) {
    let geocoder = CLGeocoder()
    
    geocoder.reverseGeocodeLocation(location, completionHandler: {
        placemarks, error in
        
        if let err = error {
            completionHandler(nil, err.localizedDescription)
        } else if let placemarkArray = placemarks {
            if let placemark = placemarkArray.first {
                completionHandler(placemark, nil)
                
            } else {
                completionHandler(nil, "Placemark was nil")
            }
        } else {
            completionHandler(nil, "Unknown error")
        }
    })
    }
}
