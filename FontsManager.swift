//
//  FontsManager.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 21.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import Foundation
import UIKit

class ImageClass: UIImageView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    func commonInit(){
        self.tintColor = UIColor.acStrawberry
       
    }
}


class MainTitleName: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    func commonInit(){
        self.font = UIFont.acTitleYesevaOne()
        self.textColor = UIColor.acCharcoalGrey
    }
}

class DescriptionLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    func commonInit(){
        self.font = UIFont.acMainDescription()
        self.textColor = UIColor.acSlateGrey
    }
}

class FilterLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    func commonInit(){
        self.font = UIFont.acMainDescription()
        self.textColor = UIColor.acSlateGrey
    }
}

class CellTitleName: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    func commonInit(){
        self.font = UIFont.acH1Font()
        self.textColor = UIColor.acCharcoalGrey
    }
}


class TypeLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    func commonInit(){
        self.font = UIFont.acCategStyleFont()
        //self.font = UIFont.acTextStyleFont()
        self.textColor = UIColor.acCoolGrey
    }
}


class DiscountLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    func commonInit(){
        self.font = UIFont.acSh3Font()
        self.textColor = UIColor.acWhiteTwo
    }
}
    
class LocationMeters: UILabel {
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.commonInit()
        }
        required init(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)!
            self.commonInit()
        }
        func commonInit(){
            self.font = UIFont.acSh3Font()
            self.textColor = UIColor.acStrawberry
        }
   }
    
class TitleCharcoalGrey: UILabel {
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.commonInit()
        }
        required init(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)!
            self.commonInit()
        }
        func commonInit(){
            self.font = UIFont.acTitelFont()
            self.textColor = UIColor.acCharcoalGrey
        }
    }

class PhoneLabel: UILabel {
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.commonInit()
        }
        required init(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)!
            self.commonInit()
        }
        func commonInit(){
            self.font = UIFont.acBasicCell()
            self.textColor = UIColor.acSlateGrey
        }
}

