//
//  FilterTableViewController.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 20.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

class FilterTableViewController: UITableViewController {
    
    
    var categories : [PlacesCategories] = []
    public var filterArray : [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadNavBar()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: Notification.Name("FilterArrayCreated"), object: self, userInfo: ["array": self.filterArray])
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return categories.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as! FilterTableViewCell
        let category = categories[indexPath.row]
        cell.createFilterCell(category: category, currentFilter: filterArray)
        
        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            
            if (cell.accessoryType == UITableViewCellAccessoryType.none) {
                
                let category = categories[indexPath.row]
                self.filterArray.append(category.id)
                cell.tintColor = UIColor.acStrawberry
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
                
                print(filterArray)
                
            }else{
                cell.accessoryType = UITableViewCellAccessoryType.none
                let category = categories[indexPath.row]
                if let indexCat = self.filterArray.index(of: category.id){
                    self.filterArray.remove(at: indexCat)
                    cell.tintColor = UIColor.white
                }
            }
            self.loadNavBar()
        }
    }
    
    func loadNavBar() {
        
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cбросить фильтр", style: .done, target: self, action: #selector(resetFilter))
        self.navigationItem.rightBarButtonItem?.isEnabled = filterArray.count > 0 ? true : false
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.acStrawberry
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.alpha = 1
    }
    
    func resetFilter () {
        self.filterArray.removeAll()
        self.tableView.reloadData()
    }
}
