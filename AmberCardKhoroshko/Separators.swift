//
//  Separators.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 27.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import Foundation
import UIKit

class Separator: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    func commonInit(){
        self.backgroundColor = UIColor.acStrawberry
    
    }
}
