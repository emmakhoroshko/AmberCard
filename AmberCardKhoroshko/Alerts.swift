//
//  Alerts.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 23.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import Foundation

class Alerts: NSObject {
    static let shared = Alerts()
    
    func AlertNoConnection(vc: UIViewController){
        
        let alert = UIAlertController(title: "Нет соединения", message: "Выключите Авиарежим или используйте Wi-Fi для доступа к данным", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ОК", style: UIAlertActionStyle.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    
    
    func AlertСantAddToFavour(vc: UIViewController){
        
        let alert = UIAlertController(title: "Нет соединения", message: "Выключите Авиарежим или используйте Wi-Fi для добавления избранного места ", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ОК", style: UIAlertActionStyle.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    func PhoneInvalide(vc: UIViewController){
        
        let alert = UIAlertController(title: "Телефонный номер некорректный", message: "Невозможно сделать вызов", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ОК", style: UIAlertActionStyle.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
 
    
}
