//
//  AllPlacesScreenCell.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 16.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

class AllPlacesScreenCell: UITableViewCell {
    
    static let shared = AllPlacesScreenCell()
    
    @IBOutlet weak var placeTypeIcon: UIImageView!
    @IBOutlet weak var placeTypeLabel: UILabel!
    @IBOutlet  var placeName: UILabel!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet var placeDescription: UILabel!
    @IBOutlet weak var metersToDestination: UILabel!
    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var discountIcon: UIImageView!
    @IBOutlet weak var discountLabel: UILabel!
    
    
    func setFonts () {
        placeDescription.font = UIFont.acTextStyleFont()
        placeName.font = UIFont.acH1Font()
    }
    
    func configurateTheCell(place: PlaceModel , category : PlacesCategories) {
        print(place.name)
        self.placeName.text = place.name == "" ? "":"\(place.name)"
        self.placeDescription.text = place.description_text == "" ? "":"\(place.description_text)"
        self.locationIcon.image = #imageLiteral(resourceName: "icLocation")
        self.locationIcon.tintColor = UIColor.acStrawberry
        self.discountLabel.text = place.discount_max == "0" ? "":"\(place.discount_max) % "
        self.likeImage.image = place.loved ? #imageLiteral(resourceName: "icSelectedLike") : #imageLiteral(resourceName: "icDefaultType")
        self.likeImage.alpha = place.loved ? 0.6 : 0
        NetworkingService.shared.downloadIcon(category.icon, defaultIcon: #imageLiteral(resourceName: "defImg"), completion: { (img) in
            self.placeTypeIcon.image = img
        })
        self.placeTypeLabel.text = category.name == "" ? "":"\(category.name)"
    }
    
}
