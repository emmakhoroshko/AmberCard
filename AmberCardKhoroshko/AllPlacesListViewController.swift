//
//  AllPlacesListViewController.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 16.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

class AllPlacesListViewController: TransparentNavigationBarVC, UITableViewDelegate,UITableViewDataSource {
    
    let APP_SCROLL_OFFSET =  -250.0
    let APP_CONTENT_INSET = 260.0
    let APP_HEIGHT_PLACE_ROW = 212
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var designView: UIView!
    @IBOutlet weak var allContentTableView: UITableView!
    
    
    var filteredArray :[Int] = []
    var points:[PlaceModel]  = []
    var categories:[PlacesCategories]  = []
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.allContentTableView.dataSource = self
        self.allContentTableView.delegate = self
        
        ConnectionCheck.shared.checkNet(viewController: self)
        
        PlacesFilteredClass.shared.fetchAllData { (success) in
            self.points = Array(PlacesFilteredClass.shared.places)
            self.categories = Array(PlacesFilteredClass.shared.categories)
            self.allContentTableView.reloadData()
            
        }
        self.changeButtonsNavBar()
        self.points = Array(PlacesFilteredClass.shared.places)
        self.categories = Array(PlacesFilteredClass.shared.categories)
        
        scrollOffset = CGPoint.init(x: 0.0, y: APP_SCROLL_OFFSET)
        self.allContentTableView.register(UINib.init(nibName: "AllPlacesScreenCell", bundle: nil), forCellReuseIdentifier: "AllPlacesScreenCell")
        self.allContentTableView.register(UINib.init(nibName: "WaitingForDataCell", bundle: nil), forCellReuseIdentifier: "WaitingForDataCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.changeButtonsNavBar()
       
        self.allContentTableView.contentInset = UIEdgeInsetsMake(CGFloat(APP_CONTENT_INSET),0,0,0);
        self.allContentTableView.reloadData()
        
        self.allContentTableView.scrollToNearestSelectedRow(at: UITableViewScrollPosition(rawValue: 0)!, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(notificationAction(notification: )), name: NSNotification.Name("FilterArrayCreated"), object: nil)
        print(points.count)
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if points.count == 0 {
         let cell: WaitingForDataCell = self.allContentTableView.dequeueReusableCell(withIdentifier: "WaitingForDataCell") as! WaitingForDataCell
            return cell
        
        }else{
        let cell: AllPlacesScreenCell = self.allContentTableView.dequeueReusableCell(withIdentifier: "AllPlacesScreenCell") as! AllPlacesScreenCell
        let place = points[indexPath.row]
        let category = self.findOutCategory(place: place)
        cell.configurateTheCell(place: place, category: category)
        return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if points.count == 0 {
            return 1
        }
        return points.count
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(APP_HEIGHT_PLACE_ROW)
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "PlaceVC", sender: indexPath)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "PlaceVC" , let vc =  segue.destination as? PlaceViewController , let indexPath =  self.allContentTableView.indexPathForSelectedRow {
            vc.place = self.points[indexPath.row]
            vc.placeType = self.findOutCategory(place: self.points[indexPath.row])
            allContentTableView.deselectRow(at: indexPath, animated: true)
        }
        if segue.identifier == "ShowFilter",let vc_filter =  segue.destination as? FilterTableViewController {
            
            vc_filter.filterArray = self.filteredArray
            let array = Array(self.categories)
            vc_filter.categories = array
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if filteredArray.count == 0 {
            if (scrollView.contentOffset.y >= scrollOffset.y) {
                self.createTransparentNavigationBar(scroll: scrollView.contentOffset)
            } else {
                self.createFullNavigationBar (scroll: scrollView.contentOffset)
            }
        }
    }
    func findOutCategory (place:PlaceModel) -> PlacesCategories{
        var categoryID: Int = 0
        var category = PlacesCategories()
        
        if filteredArray.count == 0 {
            categoryID = place.category_id[0].value
        }else{
            for i in place.category_id
            { for j in filteredArray {
                if i.value == j {
                    categoryID = i.value
                }
                }
            }
        }
        for cat in categories {
            if cat.id == categoryID {
                category = cat
            }
        }
        return category
    }
    
    func filterTapped () {
        performSegue(withIdentifier: "ShowFilter", sender: nil)
    }
    
    
    func showFavourites () {
        
        self.points = PlacesFilteredClass.shared.findFavouritePlaces()
        allContentTableView.reloadData()
        navigationController?.navigationBar.isHidden = false
        
    }
    
    func favouritesTapped () {
        self.filteredArray = []
        
        if self.navigationItem.leftBarButtonItem?.image == #imageLiteral(resourceName: "like_deselected")
        {
            self.showFavourites()
            self.navigationItem.leftBarButtonItem?.image = #imageLiteral(resourceName: "icSelectedLike")
            
            
        }else{
            self.navigationItem.leftBarButtonItem?.image = #imageLiteral(resourceName: "like_deselected")
            self.points = Array(PlacesFilteredClass.shared.places)
            allContentTableView.reloadData()
        }
        
    }
    
    
    func changeButtonsNavBar(){
        
        self.createNavigationBar()
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "like_deselected"), style: .done, target: self, action: #selector(favouritesTapped))
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Фильтровать места", style: .done, target: self, action: #selector(filterTapped))
//        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.acStrawberry
//        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.acStrawberry
        
    }
    
    func notificationAction(notification: NSNotification) {
        self.navigationItem.leftBarButtonItem?.image = #imageLiteral(resourceName: "like_deselected")
        
        if let filterArr = notification.userInfo?["array"] as? Array<Any>{
            
            self.filteredArray = filterArr as! [Int]
            if (filterArr.count > 0)&&(filterArr.count < categories.count ) {
                print("Наблюдатель увидел сообщение \(filterArr)")
                self.points = PlacesFilteredClass.shared.findPlacesWithCategory(filterArray: filterArr as! [Int])
                allContentTableView.reloadData()
                navigationController?.navigationBar.isHidden = false
            }else{
                
                self.points = Array(PlacesFilteredClass.shared.places)
                allContentTableView.reloadData()
            }
        }
    }
    
    
}
