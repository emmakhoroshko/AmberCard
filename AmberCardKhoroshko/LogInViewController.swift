//
//  LogInViewController.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 19.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

let APP_BOTTOM_CONSTRAIN = 120.0

class LogInViewController: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var bottomConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var mainImageCrown: UIImageView!
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var bgView: UIView!
    let defaults:UserDefaults = UserDefaults.standard
    
    override func viewDidLoad()  {
        
        super.viewDidLoad()
        self.keyBoardNotification()
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self
        bgView.addGestureRecognizer(tap)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.hidesBackButton = true
        self.isAppfirstTimeOpened()
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    @IBAction func checkCorrectPasswordAndMail(_ sender: UIButton) {
        let mail =  emailTextField.text
        let pass = passwordTextField.text
        
        NetworkingService.shared.loginAccept(name: mail!, password: pass!, completion: { success in
            if success {
                print("successful")
                
                self.defaults.set(true, forKey: "HasAppBeenOpenedBefore")
                self.performSegue(withIdentifier: "AllPlacesListViewController", sender: nil)
            } else {
                print("not successful")
                print("Отказано в доступе")
                self.warningAlert()
            }
        }
            
        )
    }
    
    func warningAlert(){
        let alert = UIAlertController(title: "Вход не выполнен", message: "Повторить попытку входа?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Да", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        passwordTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
    }
    
    
    @IBAction func expressEntering(_ sender: UIButton) {
        
        performSegue(withIdentifier: "AllPlacesListViewController", sender: nil)
    }
    
    
    func keyBoardNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    //  NSNotificationCenter.defaultCenter().removeObserver(self)
    // }
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize = userInfo[UIKeyboardFrameBeginUserInfoKey] as? CGRect {
                UIView.animate(withDuration: 80, animations: {
                    self.bottomConstrain.constant = keyboardSize.height
                })
            }
        }
    }
    
    
    
    func isAppfirstTimeOpened (){
        let opened = defaults.bool(forKey: "HasAppBeenOpenedBefore" )
        if opened {
            
            performSegue(withIdentifier: "AllPlacesListViewController", sender: nil)
        } else {
            ConnectionCheck.shared.checkNet(viewController: self)
            
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if  notification.userInfo  != nil {
            UIView.animate(withDuration: 8, animations: {
                self.bottomConstrain.constant = CGFloat(APP_BOTTOM_CONSTRAIN)
            })
        }
    }
}
