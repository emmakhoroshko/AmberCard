//
//  PlacesFilteredClass.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 25.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit
import RealmSwift

class PlacesFilteredClass: NSObject {
    let realm = try! Realm()
    static let shared = PlacesFilteredClass()
    public lazy var categories: Results<PlacesCategories> = { self.realm.objects(PlacesCategories.self) }()
    public lazy var places: Results<PlaceModel> = { self.realm.objects(PlaceModel.self) }()
    
    
    func findPlacesWithCategory(filterArray: [Int]) -> [PlaceModel]  {
        var filteredPlaces :[PlaceModel] = []
        for place in places {
            for _ in place.category_id.filter("value in %@",filterArray) {
                if !filteredPlaces.contains(place) {
                    filteredPlaces.append(place)
                }
                
            }
        }
        return filteredPlaces
    }
    
    func findFavouritePlaces() -> [PlaceModel]  {
        var filteredPlaces :[PlaceModel] = []
        for place in places {
            if place.loved {
                filteredPlaces.append(place)
            }
        }
        return filteredPlaces
    }
    
    func fetchAllData (completion:@escaping (Bool) -> ()) {
        NetworkingService.shared.getAllPlaces { (success) in
            if success {
                self.places = self.realm.objects(PlaceModel.self)
                self.categories = self.realm.objects(PlacesCategories.self)
                
            } else {
                print("not successful")
                print("Отказано в доступе")
            }
            completion (true)
        }
    }
    
}
