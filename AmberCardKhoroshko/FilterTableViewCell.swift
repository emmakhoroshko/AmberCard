//
//  FilterTableViewCell.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 20.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameCell: UILabel!
    @IBOutlet weak var imgCell: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func createFilterCell (category: PlacesCategories, currentFilter: [Int]) {
        self.nameCell.text = category.name
        NetworkingService.shared.downloadIcon(category.icon, defaultIcon: #imageLiteral(resourceName: "defImg"), completion: { (img) in
            self.imgCell.image = img
        })
        self.tintColor = UIColor.white
        self.accessoryType = UITableViewCellAccessoryType.none
        self.selectionStyle = UITableViewCellSelectionStyle.none
        for filter in currentFilter {
            if filter == category.id {
                self.tintColor = UIColor.acStrawberry
                self.accessoryType = UITableViewCellAccessoryType.checkmark
            }
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
