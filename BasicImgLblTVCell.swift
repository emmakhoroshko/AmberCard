//
//  BasicImgLblTVCell.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 12.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

class BasicImgLblTVCell: UITableViewCell {
    
    
   
    @IBOutlet weak var icon: UIImageView!
    
    @IBOutlet weak var cellText: UILabel!
    
    func configurateBasicCell(text: String, cellIcon: UIImage){
        
        self.icon.image = cellIcon
        self.cellText.text = text 
        
    }
    

}
