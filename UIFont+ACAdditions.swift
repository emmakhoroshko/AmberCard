//
//  UIFont+ACAdditions.swift
//
//  Generated by Zeplin on 10/17/17.
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved. 
//

import UIKit

extension UIFont {
    
	class func acCardNumberFont() -> UIFont? {
		return UIFont(name: "OpenSans", size: 24.0)
	}

	class func acH1Font() -> UIFont? {
		return UIFont(name: "YesevaOne", size: 21.0)
	}
    
    class func acTitleYesevaOne() -> UIFont? {
        return UIFont(name: "YesevaOne", size: 38.0)
    }
    
    class func acH2Font() -> UIFont? {
        return UIFont(name: "YesevaOne", size: 24.0)
    }
    
    class func acTitelFont() -> UIFont? {
        return UIFont(name: "OpenSans-Semibold", size: 16.0)
    }
	
	class func acSh3Font() -> UIFont? {
		return UIFont(name: "OpenSans-Semibold", size: 18.0)
	}

	class func acMainDescription() -> UIFont? {
		return UIFont(name: "OpenSans", size: 17.0)
	}

	class func acH3Font() -> UIFont? {
		return UIFont(name: "OpenSans-Semibold", size: 17.0)
	}

	class func acTextStyleFont() -> UIFont? {
		return UIFont(name: "OpenSans", size: 16.0)
	}
    class func acBasicCell() -> UIFont? {
        return UIFont(name: "OpenSans", size: 18.0)
    }
    class func acCategStyleFont() -> UIFont? {
        return UIFont(name: "OpenSans", size: 20.0)
    }
    
    
}
