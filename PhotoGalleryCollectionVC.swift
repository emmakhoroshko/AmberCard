//
//  PhotoGalleryCollectionVC.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 20.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

//private let reuseIdentifier = "Cell"

class PhotoGalleryCollectionVC: UICollectionViewController {
    @IBOutlet var galleryCollectionView: UICollectionView!
    
    var photos = [PhotosModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return photos.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PhotoCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionCell", for: indexPath) as! PhotoCollectionCell
        let place_img = photos[indexPath.row].photo
        cell.downloadImage(url: place_img)
        return cell
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  tmpImdex = round(Float(scrollView.contentOffset.x / UIScreen.main.bounds.size.width))
        self.updateTitle(index: Int(tmpImdex))
    }
    
    func updateTitle (index: Int ){
        self.navigationItem.title = ("\(index+1) из \(photos.count)")
    }
    
    
}
