//
//  PhotoCollectionCell.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 20.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit

class PhotoCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var image_gallery: UIImageView!
    
    func downloadImage(url:String) {
        self.image_gallery.image = #imageLiteral(resourceName: "crown")
        NetworkingService.shared.downloadIcon(url, defaultIcon: #imageLiteral(resourceName: "crown")) { (img) in
            let imageCell = img as UIImage!
            self.image_gallery.image = imageCell
        }
    }
    
    
}
