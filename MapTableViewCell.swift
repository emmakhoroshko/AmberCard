//
//  MapTableViewCell.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 19.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit


class MapTableViewCell: UITableViewCell {

    @IBOutlet weak var mapView: YMKMapView!
    var placeAnnotation = PointAnnotation()
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    func createdMapViewCell(place: PlaceModel, address: String){
        self.placeAnnotation.title_tmp = (place.name)
                    self.placeAnnotation.subTitle_tmp = address
                    self.placeAnnotation.coordinate_tmp = YMKMapCoordinateMake(place.latitude, place.longitude)
                    self.mapView.addAnnotation(placeAnnotation)
                    self.mapView.showTraffic = false
                    self.mapView.setCenter(placeAnnotation.coordinate_tmp, atZoomLevel: 15, animated: true)
 
    }
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}
