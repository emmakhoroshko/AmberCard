//
//  PlaceViewController.swift
//  AmberCardKhoroshko
//
//  Created by Emma Khoroshko on 12.10.17.
//  Copyright © 2017 Emma Khoroshko. All rights reserved.
//

import UIKit
//import Alamofire
//import AlamofireImage
//import RealmSwift



class PlaceViewController: TransparentNavigationBarVC,UITableViewDelegate,UITableViewDataSource, YMKMapViewDelegate, UIGestureRecognizerDelegate {
    
    let APP_MAP_CELL_HEIGHT =  260.0
    let APP_ESTIMATED_ROW_HEIGHT = 60.0
    
    
    
    
    @IBOutlet weak var tableViewPlaceInfo: UITableView!
    @IBOutlet weak var mainImage: UIImageView!
    var placeAnnotation = PointAnnotation()
    
    //var scrollOffset: CGPoint!
    
    enum cellNameAbbs {
        
        case BasicImgLblTVCellPhone
        case BasicImgLblTVCellAddress
        case CustomDescriptionPlaceCell
        case BasicCellCostInfo
        case BasicCellWorkingHours
        case MapTableViewCell
    }
    
    
    
    struct cellData {
        let name: cellNameAbbs
        var image: UIImage!
    }
    
    var place = PlaceModel()
    var photos = PhotosModel()
    var placeType = PlacesCategories()
    var cellsArray = [cellData]()
    var address = "..."
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.tableViewPlaceInfo.dataSource = self
        self.tableViewPlaceInfo.delegate = self
        self.tableViewPlaceInfo.register(UINib.init(nibName: "BasicImgLblTVCell", bundle: nil), forCellReuseIdentifier: "BasicImgLblTVCell")
        
        self.tableViewPlaceInfo.register(UINib.init(nibName: "CustomDescriptionPlaceCell", bundle: nil), forCellReuseIdentifier: "CustomDescriptionPlaceCell")
        
        self.tableViewPlaceInfo.register(UINib.init(nibName: "MapTableViewCell", bundle: nil), forCellReuseIdentifier: "MapTableViewCell")
        self.tableViewPlaceInfo.register(UINib.init(nibName: "TransparentCell", bundle: nil), forCellReuseIdentifier: "TransparentCell")
        
        self.setFavorIcon()
        
        self.findAddress { (result) in
            self.address = result
            self.createCellsArray()
            self.tableViewPlaceInfo.reloadData()
        }
        
        tableViewPlaceInfo.estimatedRowHeight = CGFloat(APP_ESTIMATED_ROW_HEIGHT)
        tableViewPlaceInfo.rowHeight = UITableViewAutomaticDimension
        
        self.setMainPhoto()
        self.createCellsArray()
        
//                let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
//                tap.delegate = self
//       
//                self.tableViewPlaceInfo.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.createNavigationBar()
        self.tableViewPlaceInfo.contentInset = UIEdgeInsetsMake(225,0,0,0);
    }
    
    
    
    func setMainPhoto (){
        
        self.mainImage.image = #imageLiteral(resourceName: "crown")
        let mainPhoto = place.photos[0].photo
        NetworkingService.shared.downloadIcon(mainPhoto,defaultIcon: #imageLiteral(resourceName: "crown"), completion: { (image) in
            self.mainImage.image = image
        })
        
    }
    
    func setFavorIcon (){
        if self.place.loved {
            self.addFullChangedNavController()
            
        } else {
            self.addEmptyChangedNavController()
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch cellsArray[indexPath.row].name {
            
        case cellNameAbbs.BasicImgLblTVCellPhone:
            
            let cell: BasicImgLblTVCell = self.tableViewPlaceInfo.dequeueReusableCell(withIdentifier: "BasicImgLblTVCell") as! BasicImgLblTVCell
            cell.configurateBasicCell(text: place.phone, cellIcon: #imageLiteral(resourceName: "icPhone"))
            return cell
            
        case cellNameAbbs.BasicImgLblTVCellAddress:
            
            let cell: BasicImgLblTVCell = self.tableViewPlaceInfo.dequeueReusableCell(withIdentifier: "BasicImgLblTVCell") as! BasicImgLblTVCell
            cell.configurateBasicCell(text: address, cellIcon: #imageLiteral(resourceName: "icLocation"))
            return cell
            
        case cellNameAbbs.CustomDescriptionPlaceCell:
            
            let cell: CustomDescriptionPlaceCell = self.tableViewPlaceInfo.dequeueReusableCell(withIdentifier: "CustomDescriptionPlaceCell") as! CustomDescriptionPlaceCell
            cell.createCustonCell(place: place, categ: placeType)
            
            return cell
            
        case cellNameAbbs.BasicCellCostInfo:
            let cell: TransparentCell = self.tableViewPlaceInfo.dequeueReusableCell(withIdentifier: "TransparentCell") as! TransparentCell
            let string_cell = ("\(place.cost_text) \(place.cost_sum)")
            cell.createTranspatentCell(str: string_cell)
            
            return cell
            
        case cellNameAbbs.MapTableViewCell:
            
            let cell: MapTableViewCell = self.tableViewPlaceInfo.dequeueReusableCell(withIdentifier: "MapTableViewCell") as! MapTableViewCell
            
            cell.createdMapViewCell(place: place, address: address)
            
            return cell
            
        case cellNameAbbs.BasicCellWorkingHours:
            
            let cell: TransparentCell = self.tableViewPlaceInfo.dequeueReusableCell(withIdentifier: "TransparentCell") as! TransparentCell
            cell.createTranspatentCell(str: place.description_2)
            return cell
            
        }
    }
    
    func findAddress (completionHandler: @escaping (String) -> ()){
        var address = ""
        let location = CLLocation(latitude: place.latitude, longitude: place.longitude)
        AddressDetected.shared.getPlacemark(forLocation: location, completionHandler: { (placeMark, str) in
            if placeMark != nil {
                if let addrList = placeMark?.addressDictionary?["FormattedAddressLines"] as? [String] {
                    print(addrList)
                    if  addrList.count > 1 {
                        address = ("\(addrList[0]), \(addrList[1])")
                        completionHandler(address)
                    }else{
                        address = ("\(addrList[0])")
                        completionHandler(address)
                    }
                }
            }
        })
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellsArray.count
    }
    
    
    func createCellsArray() {
        cellsArray.removeAll()
        if (place.name != "") {
            cellsArray.append(cellData(name: cellNameAbbs.CustomDescriptionPlaceCell, image: #imageLiteral(resourceName: "defImg")))
        }
        if place.description_2 != ""{
            cellsArray.append(cellData(name: cellNameAbbs.BasicCellWorkingHours, image: #imageLiteral(resourceName: "defImg")))
        }
        if (place.cost_sum != "") {
            cellsArray.append(cellData(name: cellNameAbbs.BasicCellCostInfo, image: #imageLiteral(resourceName: "defImg")))
        }
        if (place.phone != "") {
            cellsArray.append(cellData(name: cellNameAbbs.BasicImgLblTVCellPhone, image: #imageLiteral(resourceName: "defImg")))
        }
        if (place.latitude != 0) {
            cellsArray.append(cellData(name: cellNameAbbs.BasicImgLblTVCellAddress, image: #imageLiteral(resourceName: "defImg")))
        }
        cellsArray.append(cellData(name: cellNameAbbs.MapTableViewCell, image: #imageLiteral(resourceName: "defImg")))
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch cellsArray[indexPath.row].name {
            
        case cellNameAbbs.MapTableViewCell:
            return  CGFloat(APP_MAP_CELL_HEIGHT)
            
        default:
            return UITableViewAutomaticDimension
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch cellsArray[indexPath.row].name {
            
        case cellNameAbbs.BasicImgLblTVCellPhone:
            
            self.makeCall(phone: place.phone)
            
            break
            
        case cellNameAbbs.CustomDescriptionPlaceCell: break
        case cellNameAbbs.MapTableViewCell: break
        case cellNameAbbs.BasicCellWorkingHours: break
        case cellNameAbbs.BasicCellCostInfo: break
        case cellNameAbbs.BasicImgLblTVCellAddress: break

        
        default:
            
            self.handleTap()
            
            break
            
        }
        
    }
    
    func makeCall(phone: String) {
        var newPhone = ""
        for  character in phone.characters {
            
            switch (character){
            case "0","1","2","3","4","5","6","7","8","9","+" : newPhone.append(character)
            default : print("Removed invalid character.")
            }
        }
        if  (newPhone.characters.count > 1){
            UIApplication.shared.open(NSURL(string: "tel://" + newPhone)! as URL, options: [:], completionHandler: { (success) in
                print(success)
            })
        }else{
            Alerts.shared.PhoneInvalide(vc: self)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y >= scrollOffset.y) {
            self.createTransparentNavigationBar(scroll: scrollView.contentOffset)
        } else {
            self.createFullNavigationBar (scroll: scrollView.contentOffset)
        }
    }
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        performSegue(withIdentifier: "ShowGallery", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowGallery",let vc =  segue.destination as? PhotoGalleryCollectionVC {
            let array = Array(place.photos)
            vc.photos = array
            print (array)
        }
    }
    
  
    func addFullChangedNavController (){
        let like = UIBarButtonItem(image: #imageLiteral(resourceName: "icSelectedLike"), style: .done, target: self, action: #selector(self.deleteTapped))
   
        let play = UIBarButtonItem(image: #imageLiteral(resourceName: "icGallery"), style: .done, target: self, action: #selector(handleTap))
        
        navigationItem.rightBarButtonItems = [like, play]
        
        
    }
    
    func addEmptyChangedNavController (){
        let like = UIBarButtonItem(image: #imageLiteral(resourceName: "like_deselected"), style: .done, target: self, action: #selector(addTapped))
        
        let play = UIBarButtonItem(image: #imageLiteral(resourceName: "icGallery"), style: .done, target: self, action: #selector(handleTap))
        
        navigationItem.rightBarButtonItems = [like, play]
        
        
    }

    
    
    override func tappedBack () {
        self.tableViewPlaceInfo.isScrollEnabled = false
        self.navigationController!.popViewController(animated: true)
        
    }
    override func addTapped () {
        print("addTapped")
        
        NetworkingService.shared.addPlaceToFav(place: self.place) { (added) in
            
            self.addFullChangedNavController()
    }
}
    override func deleteTapped () {
        print("deleteTapped")
        NetworkingService.shared.deletePlaceFromFav(place: self.place) { (deteted) in
            self.addEmptyChangedNavController()
        }
    }
}
